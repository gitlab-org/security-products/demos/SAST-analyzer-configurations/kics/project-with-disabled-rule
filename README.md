# kics-remote-custom-ruleset

This project illustrates how to configure the `kics` SAST scanner with a custom remote ruleset.

The `main.tf` file has a known Terraform vulnerability.

The remote ruleset located in this project, [Disabled Rule Ruleset](https://gitlab.com/gitlab-org/security-products/demos/SAST-analyzer-configurations/kics/disabled-rule-ruleset),  disables this vulnerability.